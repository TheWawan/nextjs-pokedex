import Head from 'next/head'
import Link from 'next/link'
import { GetStaticProps, InferGetStaticPropsType } from 'next'

type Pokemon = {
  name: string,
  url: string
}

export const getStaticProps: GetStaticProps = async (context) => {
  let pokemons: Pokemon[]

  try {
    const res = await fetch('https://pokeapi.co/api/v2/pokemon?limit=200&offset=0')
    const {results} = await res.json()
    pokemons = results.map((pokemon, index) => {
      const paddedIndex = ('00' + (index + 1)).slice(-3)
      const image = `https://assets.pokemon.com/assets/cms2/img/pokedex/full/${paddedIndex}.png`
  
      return {
        ...pokemon,
        image
      }
    }) 
  } catch (error) {
    console.error('cannot fetch api') 
  }

  return {
    props: {
      pokemons: pokemons
    }
  }
}


function Home({ pokemons }: InferGetStaticPropsType<typeof getStaticProps>) {  
  return (
    <div className="bg-gray-800">
      <Head>
        <title>Pokedex</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <h1 className="w-full py-4 flex justify-center text-4xl text-white">
          Welcome to Pushdex
        </h1>
        <ul className="grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-3 px-6">
          {pokemons.map((pokemon, index) => (
            <li key={index} className="p-6 w-full mx-auto bg-gray-700 rounded-xl shadow-md flex items-center space-x-4 my-2">
              <Link href={`/pokemon?id=${index + 1}`}>
                <a className="w-full flex items-center justify-around capitalize">
                  <img className="h-12 w-12" src={pokemon.image} alt={pokemon.name}/>
                  <span className="text-gray-200 text-3xl">{pokemon.name}</span>
                </a>
              </Link>
            </li>
          ))}
        </ul>
      </main>

      <footer className="flex justify-center mt-6">
        <a
          className="flex"
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by {''}
          <img className="h-5" src="/vercel.svg" alt="Vercel Logo" />
        </a>
      </footer>
    </div>
  )
}

export default Home
