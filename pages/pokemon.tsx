import Link from 'next/link'
import { GetServerSideProps, GetStaticProps, InferGetStaticPropsType } from 'next'

const Pokemon = ({ pokemon }) => {
    console.log(pokemon);

    return (
        <div className="h-screen flex flex-col items-center justify-center bg-gray-800">
            <div className="absolute top-0 left-0 p-4">
                <Link href="/">
                    <a className="bg-indigo-500 text-white px-4 py-2 rounded">Home</a>
                </Link>
            </div>
            <div className="max-w-5xl">
                <div className="bg-gray-700 shadow overflow-hidden sm:rounded-lg">
                    <div className="flex items-center">
                        <img className="w-20 h-20" src={pokemon.image} alt=""/>
                        <div className="px-4 py-3 sm:px-6">
                            <h3 className="capitalize text-3xl leading-6 font-medium text-gray-200">
                                #{pokemon.id} { pokemon.name}
                            </h3>
                        </div>
                    </div>
                    <div className="border-t border-gray-200 px-4 py-5 sm:px-6">
                        <dl className="grid grid-cols-1 gap-x-4 gap-y-8 sm:grid-cols-2">
                            <div className="sm:col-span-1">
                                <dt className="text-sm font-medium text-white">
                                    Full name
                                </dt>
                                <dd className="mt-1 text-sm text-gray-200">
                                    { pokemon.name }
                                </dd>
                            </div>
                            <div className="sm:col-span-1">
                                <dt className="text-sm font-medium text-white">
                                    Base Experience
                                </dt>
                                <dd className="mt-1 text-sm text-gray-200">
                                    { pokemon.base_experience }
                                </dd>
                            </div>
                            <div className="sm:col-span-1">
                                <dt className="text-sm font-medium text-white">
                                    Weight
                                </dt>
                                <dd className="mt-1 text-sm text-gray-200">
                                    { Math.round((pokemon.weight * 2.20462262) * 10) / 100 } lbs
                                </dd>
                            </div>
                            <div className="sm:col-span-1">
                                <dt className="text-sm font-medium text-white">
                                    Types
                                </dt>
                                <dd className="mt-1 text-sm text-gray-200">
                                    <ul>
                                        { pokemon.types.map((type, index) => <p key={index}>{ type.type.name }</p>)}
                                    </ul>
                                </dd>
                            </div>
                            <div className="sm:col-span-2">
                                <dt className="text-sm font-medium text-white">
                                    About
                                </dt>
                                <dd className="mt-1 text-sm text-gray-200">
                                    Fugiat ipsum ipsum deserunt culpa aute sint do nostrud anim incididunt cillum culpa consequat. Excepteur qui ipsum aliquip consequat sint. Sit id mollit nulla mollit nostrud in ea officia proident. Irure nostrud pariatur mollit ad adipisicing reprehenderit deserunt qui eu.
                                </dd>
                            </div>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    )
}

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
    const id = parseInt(query.id)

    try {
        const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`)
        const pokemon = await res.json()
        const paddedIndex = ('00' + (id)).slice(-3)
        pokemon.image = `https://assets.pokemon.com/assets/cms2/img/pokedex/full/${paddedIndex}.png`

        return {
            props: {
                pokemon: pokemon
            }
        }
    } catch (error) {
        console.error('cannot fetch api')
    }

}

export default Pokemon
